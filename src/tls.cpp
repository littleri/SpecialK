/**
 * This file is part of Special K.
 *
 * Special K is free software : you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by The Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * Special K is distributed in the hope that it will be useful,
 *
 * But WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Special K.
 *
 *   If not, see <http://www.gnu.org/licenses/>.
 *
**/

#include <SpecialK/tls.h>
#include <SpecialK/log.h>
#include <SpecialK/config.h>
#include <SpecialK/thread.h>


volatile long __SK_TLS_INDEX = TLS_OUT_OF_INDEXES;


SK_TlsRecord
SK_GetTLS (bool initialize)
{
  DWORD dwTlsIdx =
    ReadAcquire (&__SK_TLS_INDEX);

  LPVOID lpvData =
    nullptr;

  if ( dwTlsIdx != TLS_OUT_OF_INDEXES )
  {
    lpvData =
      TlsGetValue (dwTlsIdx);

    if (lpvData == nullptr)
    {
      if (GetLastError () == ERROR_SUCCESS)
      {
        lpvData =
          static_cast <LPVOID> (
            LocalAlloc (LPTR, sizeof (SK_TLS) * SK_TLS::stack::max)
        );

        if (! TlsSetValue (dwTlsIdx, lpvData))
        {
          LocalFree (lpvData);
                     lpvData = nullptr;
        }

        else initialize = true;
      }
    }

    if (lpvData != nullptr && initialize)
    {
      SK_TLS* pTLS =
        static_cast <SK_TLS *> (lpvData);

      *pTLS = SK_TLS::SK_TLS ();

      // Stack semantics are deprecated and will be removed soon
      pTLS->stack.current = 0;
    }
  }

  else
    dwTlsIdx = TLS_OUT_OF_INDEXES;

  return
    { dwTlsIdx, lpvData };
}


void
SK_CleanupTLS (void)
{
  auto tls_slot =
    SK_GetTLS ();

  if (tls_slot.lpvData != nullptr)
  {
#ifdef _DEBUG
    size_t freed =
#endif
    static_cast <SK_TLS *> (tls_slot.lpvData)->Cleanup (
      SK_TLS_CleanupReason_e::Unload
    );

#ifdef _DEBUG
    SK_LOG0 ( ( L"Freed %zu bytes of temporary heap storage for tid=%x",
                  freed, GetCurrentThreadId () ), L"TLS Memory" );
#endif

    if (TlsSetValue (tls_slot.dwTlsIdx, nullptr))
      LocalFree     (tls_slot.lpvData);
  }
}



SK_TLS*
__stdcall
SK_TLS_Bottom (void)
{
  auto tls_slot =
    SK_GetTLS ();

  if (tls_slot.dwTlsIdx == TLS_OUT_OF_INDEXES)
    return nullptr;

  return
    static_cast <SK_TLS *> (tls_slot.lpvData);
}


SK_ModuleAddrMap::SK_ModuleAddrMap (void) = default;

bool
SK_ModuleAddrMap::contains (LPCVOID pAddr, HMODULE* phMod)
{
  if (pResolved == nullptr)
      pResolved = new std::unordered_map <LPCVOID, HMODULE> ();

  std::unordered_map <LPCVOID, HMODULE> *pResolved_ =
    ((std::unordered_map <LPCVOID, HMODULE> *)pResolved);

  const auto&& it =
    pResolved_->find (pAddr);

  if (it != pResolved_->cend ())
  {
    *phMod = (*pResolved_) [pAddr];
    return true;
  }

  return false;
}

void 
SK_ModuleAddrMap::insert (LPCVOID pAddr, HMODULE hMod)
{
  if (pResolved == nullptr)
    pResolved = new std::unordered_map <LPCVOID, HMODULE> ( );

  std::unordered_map <LPCVOID, HMODULE> *pResolved_ = 
    ((std::unordered_map <LPCVOID, HMODULE> *)pResolved);

  (*pResolved_) [pAddr] = hMod;
}


#include <SpecialK/render/d3d9/d3d9_backend.h>
#include <SpecialK/steam_api.h>

void*
SK_ImGui_ThreadContext::allocPolylineStorage (size_t needed)
{
  if (polyline_capacity < needed)
  {
    _aligned_free (polyline_storage);
                   polyline_storage = _aligned_malloc (needed, 16);

    if (polyline_storage != nullptr)
      polyline_capacity = needed;
    else
      polyline_capacity = 0;
  }

  return polyline_storage;
}

char*
SK_OSD_ThreadContext::allocText (size_t needed)
{
  if (text_capacity < needed)
  {
    _aligned_free (text);
                   text =
    (char *)_aligned_malloc (needed, 16);

    if (text != nullptr)
      text_capacity = needed;
    else
      text_capacity = 0;
  }

  return text;
}


uint8_t*
SK_RawInput_ThreadContext::allocData (size_t needed)
{
  if (capacity < needed)
  {
    _aligned_free (data);
                   data =
      (uint8_t *)_aligned_malloc (needed, 16);

    if (data != nullptr)
      capacity = needed;
    else
      capacity = 0;
  }

  return (uint8_t *)data;
}

RAWINPUTDEVICE*
SK_RawInput_ThreadContext::allocateDevices (size_t needed)
{
  if (num_devices < needed)
  {
    _aligned_free (devices);
                   devices =
    (RAWINPUTDEVICE *)_aligned_malloc (needed * sizeof (RAWINPUTDEVICE), 16);

    if (devices != nullptr)
      num_devices = needed;
    else
      num_devices = 0;
  }

  return devices;
}


size_t
SK_TLS_ScratchMemory::Cleanup (SK_TLS_CleanupReason_e /*reason*/)
{
  size_t freed = 0UL;

  freed += cmd.reclaim  ();
  freed += eula.reclaim ();

  for ( auto* segment : { &ini.key, &ini.val, &ini.sec } )
    freed += segment->reclaim ();

  return freed;
}

size_t
SK_ImGui_ThreadContext::Cleanup (SK_TLS_CleanupReason_e /*reason*/)
{
  size_t freed = 0;

  if (polyline_storage != nullptr)
  {
    freed += polyline_capacity;

    _aligned_free (polyline_storage);
                   polyline_storage = nullptr;

             polyline_capacity = 0;
  }

  return freed;
}

size_t
SK_OSD_ThreadContext::Cleanup (SK_TLS_CleanupReason_e /*reason*/)
{
  size_t freed = 0;

  if (text != nullptr)
  {
    freed += text_capacity;

    _aligned_free (text);
                   text = nullptr;

          text_capacity = 0;
  }

  return freed;
}

size_t
SK_RawInput_ThreadContext::Cleanup (SK_TLS_CleanupReason_e /*reason*/)
{
  size_t freed = 0;

  if (data != nullptr)
  {
    freed += capacity;

    _aligned_free (data);
                   data = nullptr;

          capacity = 0;
  }

  if (devices != nullptr)
  {
    freed += num_devices * sizeof (RAWINPUTDEVICE);

    _aligned_free (devices);
                   devices = nullptr;

          num_devices = 0;
  }

  return freed;
}

size_t
SK_Steam_ThreadContext::Cleanup (SK_TLS_CleanupReason_e reason)
{
  size_t freed = 0;

  if (reason == Unload)
  {
    if (steam_ctx.Utils () != nullptr && client_user != 0 && reason == Unload)
    {
      if (steam_ctx.ReleaseThreadUser ())
      {
      //freed += 4;

        client_user = 0;
      }
    }

    if (steam_ctx.Utils () != nullptr && client_pipe != 0 && reason == Unload)
    {
      if (steam_ctx.ReleaseThreadPipe ())
      {
      //freed += 4;

        client_pipe = 0;
      }
    }
  }

  return freed;
}

#include <SpecialK/render/backend.h>

int
SK_Win32_ThreadContext::getThreadPriority (bool nocache)
{
  const DWORD _RefreshInterval = 30000UL;

  if (nocache)
  {
    thread_prio            = GetThreadPriority (SK_GetCurrentThread ());
    last_tested_prio.time  = timeGetTime       ();
    last_tested_prio.frame = SK_GetFramesDrawn ();

    return thread_prio;
  }

  if (last_tested_prio.frame < SK_GetFramesDrawn ())
  {
    DWORD dwNow = timeGetTime ();

    if (last_tested_prio.time < dwNow - _RefreshInterval)
    {
      thread_prio            = GetThreadPriority (SK_GetCurrentThread ());
      last_tested_prio.time  = dwNow;
      last_tested_prio.frame = SK_GetFramesDrawn ();
    }
  }

  return thread_prio;
}

void*
SK_D3D9_ThreadContext::allocTempFullscreenStorage (size_t /*dontcare*/)
{
  if (temp_fullscreen == nullptr)
  {
    temp_fullscreen = new D3DDISPLAYMODEEX { };
  }

  return temp_fullscreen;
}

void*
SK_D3D9_ThreadContext::allocStackScratchStorage (size_t size)
{
  if (stack_scratch.storage == nullptr)
  {
    stack_scratch.storage =
                         _aligned_malloc (size, 16);
    RtlZeroMemory (stack_scratch.storage, size);
  }

  else
  {
    if (stack_scratch.size < size)
    {
      _aligned_free (stack_scratch.storage);
                     stack_scratch.storage = _aligned_malloc (size, 16);
                     stack_scratch.size    =        (uint32_t)size ;

      RtlZeroMemory (stack_scratch.storage, size);
    }
  }

  return stack_scratch.storage;
}

size_t
SK_D3D9_ThreadContext::Cleanup (SK_TLS_CleanupReason_e /*reason*/)
{
  size_t freed = 0;

  if (stack_scratch.storage != nullptr)
  {
    _aligned_free (stack_scratch.storage);
                   stack_scratch.storage = nullptr;

    freed += stack_scratch.size;
             stack_scratch.size = 0;
  }

  if (temp_fullscreen != nullptr)
  {
    delete temp_fullscreen;
           temp_fullscreen = nullptr;

           freed += sizeof (D3DDISPLAYMODEEX);
  }

  return freed;
}



extern SK_D3D11_Stateblock_Lite*
SK_D3D11_AllocStateBlock (size_t& size);

extern void
SK_D3D11_FreeStateBlock (SK_D3D11_Stateblock_Lite* sb);

SK_D3D11_Stateblock_Lite*
SK_D3D11_ThreadContext::getStateBlock (void)
{
  if (stateBlock == nullptr)
  {
    stateBlock =
      SK_D3D11_AllocStateBlock (stateBlockSize);
  }

  return stateBlock;
}

size_t
SK_D3D11_ThreadContext::Cleanup (SK_TLS_CleanupReason_e /*reason*/)
{
  size_t freed = 0;

  if (stateBlock != nullptr)
  {
    SK_D3D11_FreeStateBlock (stateBlock);
                             stateBlock = nullptr;

    freed += stateBlockSize;
             stateBlockSize = 0;
  }

  return freed;
}


size_t
SK_TLS::Cleanup (SK_TLS_CleanupReason_e reason)
{
  size_t freed = 0UL;

  freed += d3d9          .Cleanup (reason);
  freed += imgui         .Cleanup (reason);
  freed += osd           .Cleanup (reason);
  freed += raw_input     .Cleanup (reason);
  freed += scratch_memory.Cleanup (reason);
  freed += steam         .Cleanup (reason);
  freed += d3d11         .Cleanup (reason);


  if (known_modules.pResolved != nullptr)
  {
    freed +=
      sizeof HMODULE *
        ((std::unordered_map <LPCVOID, HMODULE> *)known_modules.pResolved)->size () * 2;

    delete known_modules.pResolved;
           known_modules.pResolved = nullptr;
  }

  // Include the size of the TLS data structure on thread unload
  if (reason == Unload)
    freed += sizeof (SK_TLS);

  return freed;
}